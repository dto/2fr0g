* TODO add babel/usocket/zlib/salza2/chipz libs/licenses

works on yosemite 10.3

** TODO add hippo
*** TODO hippo comes from bottom edge of screen and moves upward to between 40-70% of the screen height and somewhat side to side 
*** TODO hippo disappears if touching lilypad, but waves drawn anyway

** TODO lower value of water a shade to improve contrast
** TODO check for fly carrier / distant ball when guard near goal 
** TODO guard plays goalie when near enough to wrongly knock ball carrier / ball into mama

** DONE make pond with invisible border wall
   CLOSED: [2015-12-24 Thu 18:30]
** TODO short tap makes short jump
** TODO long tap makes long jump
** DONE require AI use quantized (rounded) 8-dir
   CLOSED: [2015-12-24 Thu 21:37]
** TODO choose 4 progressively darker pondwater colors plus black, via M-x list-colors-display
** TODO procedural lilypads
** TODO neutral-zone is the invisible frog-width stripe going from 0,0 to 1280,720 (or opposite corners)
** TODO symmetrical procedural fields of lilypads
** TODO procedural islands
** TODO add fly wander
** TODO add fly island 
** DONE decide how collision with water (background) is to be detected
   CLOSED: [2015-12-24 Thu 18:35]
*** DONE perhaps water-p will return true when not touching any lilypad 
    CLOSED: [2015-12-24 Thu 18:35]
** TODO jump from lily pad to lily pad successfully
** TODO draw wave for showing splash location 
** TODO splash into water
** TODO move slowly when in water
** TODO test 8-way movement/jumping 
** TODO place fly
** TODO fly wanders
** TODO catch fly
** TODO drop fly when falling into water
** TODO fly slows down carrier
** TODO add simple AI 2nd frog
** TODO knock away fly when touch opponent
** TODO add mamas 
** TODO add teammates
** TODO add passing to teammate 
** TODO add leader AI for 3rd frog
** TODO add follower AI for 4th frog (then 2nd)
** TODO different things have different friction amounts, but these are ignored whhile the jump timer is "up"
** TODO display game clock
** TODO scoring
** TODO display score near mama
** TODO display icon/text for status of each frog in 4 consistent spots
** TODO use visual icons for hearing impaired 
*** TODO make croak.wav
*** TODO make cricket-1.wav cricket-2.wav cricket-3.wav 
*** TODO peep-1.wav peep-2.wav peep-3.wav peep-4.wav as distinct player jump pitches 
*** TODO corresponding .pngs for these
*** TODO grab.png, go.png, jump.png
*** TODO fly icon just flashes for serve
** TODO endgame/nightfall
** TODO setup screen
** TODO kids mode

* Archived Entries

** DONE fix frog-stand.png to have meatier legs that match the frog-jump.png
   CLOSED: [2015-12-24 Thu 03:03]
   :PROPERTIES:
   :ARCHIVE_TIME: 2015-12-24 Thu 09:10
   :ARCHIVE_FILE: ~/2fr0g/tasks.org
   :ARCHIVE_CATEGORY: tasks
   :ARCHIVE_TODO: DONE
   :END:

** DONE draw fly-1 fly-2 fly-3
   CLOSED: [2015-12-24 Thu 03:03]
   :PROPERTIES:
   :ARCHIVE_TIME: 2015-12-24 Thu 09:10
   :ARCHIVE_FILE: ~/2fr0g/tasks.org
   :ARCHIVE_CATEGORY: tasks
   :ARCHIVE_TODO: DONE
   :END:

** DONE export images from pond.xcf
   CLOSED: [2015-12-24 Thu 03:05]
   :PROPERTIES:
   :ARCHIVE_TIME: 2015-12-24 Thu 09:10
   :ARCHIVE_FILE: ~/2fr0g/tasks.org
   :ARCHIVE_CATEGORY: tasks
   :ARCHIVE_TODO: DONE
   :END:

** DONE build image-set vars
   CLOSED: [2015-12-24 Thu 09:10]
   :PROPERTIES:
   :ARCHIVE_TIME: 2015-12-24 Thu 09:10
   :ARCHIVE_FILE: ~/2fr0g/tasks.org
   :ARCHIVE_CATEGORY: tasks
   :ARCHIVE_TODO: DONE
   :END:

** DONE draw wave.png
   CLOSED: [2015-12-24 Thu 09:54]
   :PROPERTIES:
   :ARCHIVE_TIME: 2015-12-24 Thu 09:54
   :ARCHIVE_FILE: ~/2fr0g/tasks.org
   :ARCHIVE_CATEGORY: tasks
   :ARCHIVE_TODO: DONE
   :END:

