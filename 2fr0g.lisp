(in-package :2fr0g)

(defclass ip-prompt (prompt)
  ((prompt-string :initform "Type the IP server address and then press ENTER.")))

(defmethod read-expression ((prompt ip-prompt) input-string)
  input-string)

(defmethod enter :before ((prompt ip-prompt) &optional no-clear)
  (handler-case 
      (let ((*read-eval* nil))
	(let ((result (parse-ip (slot-value prompt 'line))))
	  (if (null result)
	      (logging "Error: not a valid IP address.")
	      (progn 
		(setf *server-host* (reformat-ip result))
		(start-client (current-buffer))))))
    (condition (c)
      (logging "~S" c))))

(defun show-prompt ()
  (show-terminal)
  (setf *prompt* (make-instance 'ip-prompt))
  (move-to *prompt* *terminal-left* *terminal-bottom*))

(defun hide-prompt ()
  (setf *prompt* nil))

(defparameter *2fr0g-copyright-notice*
"
 #####                  ###          
#     # ###### #####   #   #   ####  
      # #      #    # #   # # #    # 
 #####  #####  #    # #  #  # #      
#       #      #####  # #   # #  ### 
#       #      #   #   #   #  #    # 
####### #      #    #   ###    ####  
-----------------------------------------------------------------
Welcome to 2fr0g. 
2fr0g and Xelf are Copyright (C) 2006-2016 by David T. O'Toole 
email: <dto@xelf.me>   website: http://xelf.me/

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 

Full license text of the GNU Lesser General Public License is in the
enclosed file named 'COPYING'. Full license texts for compilers,
assets, libraries, and other items are contained in the LICENSES
directory included with this application.
-----------------------------------------------------------------
")

;;; Kids mode

(defvar *kids-mode* nil)
(defparameter *teddy-image* "teddy.png")
(defparameter *teddy-color* "yellow")

(defun toggle-kids-mode ()
  (setf *kids-mode* (if *kids-mode* nil t)))

(defun kids-mode-p ()
  (not (null *kids-mode*)))

;;; Game clock and scoring

(defparameter *game-length* (minutes 4))

(defvar *game-clock* 0)

(defun reset-game-clock ()
  (setf *hippo-times* (random-hippo-times))
  (setf *game-clock* *game-length*))

(defun update-game-clock ()
  (when (plusp *game-clock*)
    (decf *game-clock*)))

(defun game-on-p () 
  (plusp *game-clock*))

(defun game-clock () *game-clock*)

(defun game-clock-string (&optional (clock *game-clock*))
  (let ((minutes (/ clock (minutes 1)))
	(seconds (/ (rem clock (minutes 1)) 
		    (seconds 1))))
    (format nil "~D~A~2,'0D" (truncate minutes) ":" (truncate seconds))))

(defvar *score-1* 0)
(defvar *score-2* 0)

(defvar *scoring-team* 0)

(defun reset-score () (setf *score-1* 0 *score-2* 0))

;;; Globals 

(defparameter *paused* nil)
(defparameter *width* 1280)
(defparameter *height* 720)

(defun width-in-units () (truncate (/ *width* *unit*)))
(defun height-in-units () (truncate (/ *height* *unit*)))

(defparameter *grid-directions* '(:right :up :left :down))
(defparameter *grid-diagonals* '(:upright :upleft :downright :downleft))

(defvar *difficult* nil)
(defun difficult-p () *difficult*)
(defvar *red-green-color-blindness* nil)

(defvar *lilypads* nil)
(defvar *islands* nil)

;;; Colors 

(defparameter *neutral-color* "white")

(defparameter *traditional-frog-colors* 
  '("gold" "olive drab" "RoyalBlue3" "dark orchid"))
(defparameter *pastel-frog-colors* 
  '("yellow" "pale turquoise" "pink" "pale green"))

(defparameter *player-1-color* "gold")
(defparameter *player-2-color* "olive drab")
(defparameter *player-3-color* "RoyalBlue3")
(defparameter *player-4-color* "dark orchid")

(defun install-frog-colors (&optional (colors *pastel-frog-colors*))
  (destructuring-bind (a b c d) colors
    (setf *player-1-color* a 
	  *player-2-color* b
	  *player-3-color* c
	  *player-4-color* d)))

(defun mama-1-color () *player-1-color*)
(defun mama-2-color () *player-3-color*)

(defparameter *hippo-color* "HotPink1")
(defparameter *lilypad-colors* '("lime green"))
(defparameter *fly-color* "white")
(defparameter *island-color* "goldenrod")

(defparameter *pond-colors*
  '("LightSlateBlue" "MediumSlateBlue" "SlateBlue" 
    "DarkSlateBlue" "MidnightBlue")) 

;;; Sprites

(defun image-set (name count &optional (start 1))
  (loop for n from start to count
	collect (format nil "~A-~S.png" name n)))

(defparameter *frog-image* "frog.png")
(defparameter *frog-jump-image* "frog-jump.png")
(defparameter *mama-image* "frog.png")
(defparameter *fly-images* (image-set "fly" 3))
(defparameter *lilypad-images* (image-set "lilypad" 4))
(defparameter *lilypad-small-images* (image-set "lilypad-small" 3))
(defparameter *island-images* (image-set "island" 4))
(defparameter *hippo-image* "hippo.png")
(defparameter *teddy-bear-image* "teddy-bear.png")
(defparameter *wave-image* "wave.png")

;;; Sounds

(defresource "fanfare-1.wav" :volume 250)
(defresource "fanfare-2.wav" :volume 250)
(defresource "fanfare-3.wav" :volume 250)

(defresource 
      (:name "boop1.wav" :type :sample :file "boop1.wav" :properties (:volume 50))
      (:name "boop2.wav" :type :sample :file "boop2.wav" :properties (:volume 50))
    (:name "boop3.wav" :type :sample :file "boop3.wav" :properties (:volume 50)))

(defparameter *bounce-sounds* '("boop1.wav" "boop2.wav" "boop3.wav"))

(defresource 
    (:name "doorbell1.wav" :type :sample :file "doorbell1.wav" :properties (:volume 23))
    (:name "doorbell2.wav" :type :sample :file "doorbell2.wav" :properties (:volume 23))
  (:name "doorbell3.wav" :type :sample :file "doorbell3.wav" :properties (:volume 23)))

(defparameter *doorbell-sounds* '("doorbell1.wav" "doorbell2.wav" "doorbell3.wav"))

(defparameter *slam-sounds*
  (defresource 
      (:name "slam1.wav" :type :sample :file "slam1.wav" :properties (:volume 52))
      (:name "slam2.wav" :type :sample :file "slam2.wav" :properties (:volume 52))
    (:name "slam3.wav" :type :sample :file "slam3.wav" :properties (:volume 52))))

(defresource 
    (:name "whack1.wav" :type :sample :file "whack1.wav" :properties (:volume 42))
    (:name "whack2.wav" :type :sample :file "whack2.wav" :properties (:volume 42))
  (:name "whack3.wav" :type :sample :file "whack3.wav" :properties (:volume 42)))

(defparameter *whack-sounds* '("whack1.wav" "whack2.wav" "whack3.wav"))

(defresource 
    (:name "color1.wav" :type :sample :file "color1.wav" :properties (:volume 32))
    (:name "color2.wav" :type :sample :file "color2.wav" :properties (:volume 32))
  (:name "color3.wav" :type :sample :file "color3.wav" :properties (:volume 32)))

(defparameter *color-sounds* '("color1.wav" "color2.wav" "color3.wav" "color4.wav"))

(defresource "applause.wav" :volume 50)
(defresource "whistle.wav" :volume 80)
(defresource "bleep.wav" :volume 70)
(defresource "bloop.wav" :volume 70)
(defresource "buzz.wav" :volume 30)
(defresource "go.wav" :volume 170)
(defresource "grab.wav" :volume 70)
(defresource "hole.wav" :volume 40)
(defresource "phasebell.wav" :volume 70)
(defresource "plasma.wav" :volume 70)
(defresource "powerup.wav" :volume 70)
(defresource "serve.wav" :volume 70)
(defresource "upwoop.wav" :volume 70)
(defresource "bounce.wav" :volume 60)
(defresource "newball.wav" :volume 60)
(defresource "return.wav" :volume 60)
(defresource "error.wav" :volume 60)

;;; Main entities

(defvar *pond* nil)
(defun pond () *pond*)

(defvar *player-1* nil)
(defvar *player-2* nil)
(defvar *player-3* nil)
(defvar *player-4* nil)

(defun player-1 () *player-1*)
(defun player-2 () *player-2*)
(defun player-3 () *player-3*)
(defun player-4 () *player-4*)

(defun set-player-1 (x) (setf *player-1* x))
(defun set-player-2 (x) (setf *player-2* x))
(defun set-player-3 (x) (setf *player-3* x))
(defun set-player-4 (x) (setf *player-4* x))

(defun player-1-p (x) (eq (find-object x) (find-object *player-1*)))
(defun player-2-p (x) (eq (find-object x) (find-object *player-2*)))
(defun player-3-p (x) (eq (find-object x) (find-object *player-3*)))
(defun player-4-p (x) (eq (find-object x) (find-object *player-4*)))

(defvar *mama-1* nil)
(defvar *mama-2* nil)

(defun mama-1 () *mama-1*)
(defun mama-2 () *mama-2*)

(defun set-mama-1 (x) (setf *mama-1* x))
(defun set-mama-2 (x) (setf *mama-2* x))

;;; Controls

(defvar *player-1-joystick* 0)
(defvar *player-2-joystick* nil)
(defvar *player-3-joystick* nil)
(defvar *player-4-joystick* nil)

;;; Physics 

(defclass thing (node)
  ((color :initform *neutral-color*)
   (heading :initform 0.0)
   ;; thrust magnitudes
   (tx :initform 0.0)
   (ty :initform 0.0)
   ;; physics vars
   (dx :initform 0.0)
   (dy :initform 0.0)
   (ddx :initform 0.0)
   (ddy :initform 0.0)
   ;; physics params
   (max-dx :initform 100)
   (max-dy :initform 100)
   (max-ddx :initform 4)
   (max-ddy :initform 4)))

;;; Movement 

(defmethod face ((thing thing) direction)
  (setf (slot-value thing 'heading) 
	(direction-heading direction))
  (update-heading thing))

(defparameter *dead-zone* 0.1)

(defmethod at-rest-p ((thing thing))
  (with-slots (dx dy) thing
    (and (> *dead-zone* (abs dx))
	 (> *dead-zone* (abs dy)))))

(defmethod slow-p ((thing thing))
  (with-slots (dx dy) thing
    (and (> 1 (abs dx))
	 (> 1 (abs dy)))))

(defmethod knock-toward-center ((thing thing))
  (multiple-value-bind (gx gy) (center-point thing)
    (multiple-value-bind (cx cy) (center-point (current-buffer))
      (let ((jerk-distance (/ (distance cx cy gx gy) 16)))
	(with-slots (heading) thing
	  (setf heading (find-heading gx gy cx cy))
	  (move thing heading jerk-distance))))))

(defmethod center-on ((thing thing) cx cy)
  (with-slots (height width) thing
    (move-to thing (- cx (/ width 2)) (- cy (/ height 2)))))

(defmethod spawn-position ((thing thing))
  (values (/ *width* 2) (/ *height* 2)))

(defmethod spawn ((thing thing))
  (multiple-value-bind (x y) (spawn-position thing)
    (center-on thing x y)))

(defmethod restrict-to-buffer ((thing thing))
  (unless (bounding-box-contains (multiple-value-list (bounding-box (current-buffer)))
				 (multiple-value-list (bounding-box thing)))
    (reset-physics thing)
    (knock-toward-center thing)))

(defmethod deploy ((thing thing)) 
  (when (not (xelf::contains-node-p (pond) thing))
    (progn (insert thing)
	   (spawn thing))))

(defmethod handle-collision ((u thing) (v thing))
  (collide u v)
  (collide v u))

(defmethod max-speed ((thing thing)) (slot-value thing 'max-dx))
(defmethod max-acceleration ((thing thing)) (slot-value thing 'max-ddx))

(defparameter *thrust* 0.3)

(defmethod center-of-pond ()
  (values (/ *width* 2) (/ *height* 2)))

(defmethod heading-to-center ((thing thing))
  (multiple-value-bind (tx ty) (center-point thing)
    (multiple-value-bind (cx cy) (center-of-pond)
      (find-heading tx ty cx cy))))
  
(defun clamp (x bound)
  (max (- bound)
       (min x bound)))

(defun clamp0 (x bound)
  (let ((value (clamp x bound)))
    (if (< (abs value) *dead-zone*)
	0
	value)))

(defmethod decay ((thing thing) x)
  (let ((z (* 0.92 x)))
    z))

(defmethod aim-heading ((self thing)) nil)

(defmethod movement-heading ((self thing))
  (aim-heading self))

(defmethod current-heading ((self thing)) 
  (slot-value self 'heading))

(defmethod thrust-x ((self thing)) 
  (when (movement-heading self) *thrust*))

(defmethod thrust-y ((self thing)) 
  (when (movement-heading self) *thrust*))
      
(defmethod update-thrust ((self thing))
  (with-slots (tx ty) self
    (let ((heading (current-heading self))
	  (thrust-x (thrust-x self))
	  (thrust-y (thrust-y self)))
      (setf tx (if thrust-x (* thrust-x (cos heading)) nil))
      (setf ty (if thrust-y (* thrust-y (sin heading)) nil)))))

(defmethod reset-physics ((self thing))
  (with-slots (dx dy ddx ddy) self
    (setf dx 0 dy 0 ddx 0 ddy 0)))

(defmethod impel ((self thing) &key speed heading)
  (with-slots (tx ty dx dy ddx ddy) self
    (setf (slot-value self 'heading) heading)
    (setf ddx 0 ddy 0)
    (setf dx (* speed (cos heading)))
    (setf dy (* speed (sin heading)))))
          
(defmethod repel ((this thing) (that thing) &optional (speed 30))
  (impel that :speed speed :heading (heading-between this that)))

(defmethod update-physics ((self thing))
  (with-slots (x y dx dy ddx ddy tx ty
		  max-dx max-ddx max-dy max-ddy) self
    (setf ddx (clamp (or tx (decay self ddx))
		     (max-acceleration self)))
    (setf dx (clamp (if tx (+ dx ddx) (decay self dx))
		    (max-speed self)))
    (setf ddy (clamp (or ty (decay self ddy))
		     (max-acceleration self)))
    (setf dy (clamp (if ty (+ dy ddy) (decay self dy))
		    (max-speed self)))))

(defmethod update-position ((self thing))
  (with-slots (x y dx dy) self
    (move-to self 
	     (+ x dx)
	     (+ y dy))))

(defmethod update-heading ((self thing))
  (with-slots (heading) self
    (let ((result (or (aim-heading self) heading)))
      (when result (setf heading result)))))

(defmethod update :before ((thing thing))
  (unless (eq :passive (slot-value thing 'collision-type))
    (update-thrust thing)
    (update-physics thing)
    (update-position thing)
    (update-heading thing)))

(defmethod draw-normally ((thing thing))
  (with-slots (color image x y width height) thing
    (draw-textured-rectangle-* x y 0 width height
				 (find-texture image)
				 :vertex-color color
				 :blend :alpha)))
    
(defmethod draw ((self thing))
  (with-slots (color image heading) self
    (multiple-value-bind (top left right bottom)
	(bounding-box self)
      (draw-textured-rectangle-* left top 0
				 (- right left) (- bottom top)
				 (find-texture image)
				 :vertex-color color
				 :blend :alpha
				 ;; adjust angle to normalize for up-pointing sprites 
				 :angle (+ 90 (heading-degrees heading))))))

(defmethod team-number ((thing thing)) 1)

;;; The bouncing Squarefly

(defparameter *fly-size* (units 0.5))
(defparameter *kick-disabled-time* 40)

(defparameter *fly-radius* (units 0.5))

(defvar *fly* nil)
(defun fly () *fly*)
(defun set-fly (x) (setf *fly* x))

(defun random-serve-heading ()
  (direction-heading (random-choose '(:upleft :downright))))

(defun random-fly-image () (random-choose *fly-images*))

(defclass fly (thing)
  ((max-dx :initform 100)
   (max-dy :initform 100)
   (max-ddx :initform 0.01)
   (max-ddy :initform 0.01)
   (image :initform (random-fly-image))
   (kick-clock :initform 0)
   (color :initform *neutral-color*)
   (heading :initform (random-serve-heading))))

(defmethod spawn-position ((fly fly))
  (values (/ *width* 2) (/ *height* 2)))

(defmethod draw :around ((fly fly))
  (unless *reset-clock*
    (call-next-method)))

(defmethod go-to ((fly fly) x y)
  (move-to fly (- x (/ *fly-size* 2)) (- y (/ *fly-size* 2))))

(defmethod initialize-instance :after ((fly fly) &key)
  (setf *fly* fly)
  (resize fly *fly-size* *fly-size*))

(defmethod paint ((fly fly) color)
  (setf (slot-value fly 'color) color))

(defmethod disable-kicking ((fly fly))
  (setf (slot-value fly 'kick-clock) *kick-disabled-time*))

(defmethod recently-kicked-p ((fly fly))
  (plusp (slot-value fly 'kick-clock)))

(defmethod bounce ((fly fly) &optional (speed 5))
  (with-slots (heading) fly
    (free-fly)
    (reset-physics fly)
    (setf heading (opposite-heading heading))
    (move fly heading 8)
    (impel fly :speed speed :heading heading)))

(defmethod update ((fly fly))
  (with-slots (x y kick-clock image heading speed color) fly
    (setf image (random-choose *fly-images*))
    (when (plusp kick-clock)
      (decf kick-clock))
    (restrict-to-buffer fly)))

;;; Walls and bricks

(defclass wall (thing)
  ((color :initform "gray50")))

(defmethod draw ((wall wall)) nil)
   
(defmethod collide ((fly fly) (wall wall))
  (unless (fly-carrier) 
    (bounce fly)))

(defparameter *brick-width* (units 1.8))
(defparameter *brick-height* (units 1.2))

(defclass brick (thing)
  ((collision-type :initform :passive)
   (color :initform "white")
   (height :initform *brick-height*)
   (width :initform *brick-width*)))

(defmethod handle-collision ((this brick) (that brick)) nil)

(defmethod collide ((fly fly) (brick brick))
  (play-sample (random-choose *color-sounds*))
  (bounce fly 10))

(defmethod draw ((brick brick))
  (with-slots (x y width height color) brick
    (draw-box x y width height :color color)))

(defun make-brick (x y &optional (color "cyan"))
  (let ((brick (make-instance 'brick)))
    (resize brick *brick-width* *brick-height*)
    (move-to brick x y)
    (setf (slot-value brick 'color) color)
    brick))

(defun make-column (x y count &optional (color "cyan"))
  (with-new-buffer
    (dotimes (n count)
      (add-node (current-buffer) (make-brick x y color) x y)
      (incf y *brick-height*))
    (current-buffer)))

;;; A froggy, either human or AI controlled

(defparameter *frog-size* (units 1))

(defparameter *max-frog-speed* 8.9)
(defparameter *max-carry-speed* 5.0)
(defparameter *max-water-speed* 6)

(defvar *serve-period-timer* 0)
(defparameter *serve-period* 55)

(defun update-serve-period-timer ()
  (when (plusp *serve-period-timer*)
    (decf *serve-period-timer*)))

(defun serve-period-p ()
  (plusp *serve-period-timer*))

(defun begin-serve-period ()
  (setf *serve-period-timer* *serve-period*))

(defclass frog (thing)
  ((max-dx :initform *max-frog-speed*)
   (max-dy :initform *max-frog-speed*)
   (max-ddx :initform 1.5)
   (max-ddy :initform 1.5)
   (input-plist :initform nil :accessor input-plist)
   (input-p :initform nil :accessor input-p)
   (input-time :initform nil :accessor input-time)
   (input-update-p :initform nil :accessor input-update-p)
   (input-direction :initform nil :accessor input-direction)
   (input-jumping-p :initform nil :accessor input-jumping-p)
   (home-clock :initform 0)
   (image :initform "frog.png")
   (color :initform *neutral-color*)
   (carrying :initform nil)
   (jump-clock :initform 0 :accessor jump-clock)
   (pad-count :initform 1 :accessor pad-count)))

(defmethod update :before ((frog frog))
  (with-slots (home-clock) frog
    (when (plusp home-clock)
      (decf home-clock))))

(defmethod home-to-mama-p ((frog frog))
  (plusp (slot-value frog 'home-clock)))

(defmethod collide :around ((frog frog) (fly fly))
  (multiple-value-bind (x y) (center-point frog)
    (multiple-value-bind (x0 y0) (center-point fly)
      (when (< (distance x0 y0 x y) *fly-radius*)
	(call-next-method)))))

(defmethod decay ((frog frog) x)
  (let ((factor (if (waterp frog) 0.85 0.92)))
    (* factor x)))

(defmethod draw ((frog frog))
  (with-slots (color image heading x y) frog
    (multiple-value-bind (top left right bottom)
	(bounding-box frog)
      (let ((bump (if (slow-p frog) 0 (units 0.2))))
	(draw-textured-rectangle-* (- left bump) (- top bump) 0
				   (+ (- right left) (* bump 2))
				   (+ (- bottom top) (* bump 2))
				   (find-texture image)
				 :vertex-color color
				 :blend :alpha
				 ;; adjust angle to normalize for up-pointing sprites 
				 :angle (+ 90 (heading-degrees heading)))))))

(defmethod bounce-home-to-mama ((frog frog))
  (with-slots (home-clock) frog
    (setf home-clock (seconds (if (kids-mode-p) 1.0 3)))
    (play-sample "hole.wav")
    (when (carrying-fly-p frog)
      (lose-fly frog))
    (impel frog :heading (+ (heading-between frog (find-mama frog))
			    (* 4 (random-choose '(0.2 -0.2 0.3 -0.3 0.4 -0.4))))
			    :speed 18)))

(defmethod movement-heading ((frog frog)) nil)

(defun find-hippo ()
  (first (find-instances (pond) 'hippo)))

(defvar *hippo* nil)
(defun hippo () *hippo*)

(defmethod near-hippo-p ((frog frog))
  (let ((hippo (find-hippo)))
    (when hippo
      (< (distance-between hippo frog) 120))))

(defmethod lilypad-heading-weight ((frog frog))
  (if (lilypad-heading frog)
      (if (kids-mode-p) 0.3 0.44)
      1))

(defmethod aim-heading :around ((frog frog))
  (cond 
    ((home-to-mama-p frog)
     (direction-heading (find-heading-direction (- (random (* 2 pi)) pi))))
    ((humanp frog)
     (or (stick-heading frog)
	 ;; use last value
	 (slot-value frog 'heading)))
    ((null (fly-carrier))
     (direction-heading (find-heading-direction (heading-to-fly frog))))
    (t
     (let ((result (call-next-method)))
       (when result
	 (if (and (not (humanp frog))
		  (near-hippo-p frog))
	     (heading-between (first (find-instances (pond) 'hippo)) frog)
	     ;; quantize to 8-way
	     (if (and (not (humanp frog)) 
		      (carrying-fly-p frog)
		      (not (near-mama-p frog)))
		 (quantize-heading
		  (/ (+ (heading-between frog (find-mama frog))
			(* (lilypad-heading-weight frog) 
			   (or (lilypad-heading frog) 0.1)))
		     2))
		 (call-next-method)))
	      (direction-heading (find-heading-direction result)))))))

(defmethod find-mama ((frog frog)) (mama-1))

(defmethod find-grab-sound ((frog frog)) "color1.wav")

(defmethod carrying-fly-p ((frog frog))
  (slot-value frog 'carrying))

(defmethod reset-pad-count ((frog frog))
  (setf (pad-count frog) 0))

(defmethod reset-pad-count-maybe ((frog frog))
  (when (slow-p frog)
    (reset-pad-count frog)))

(defmethod increase-pad-count ((frog frog))
  (incf (pad-count frog)))

(defmethod waterp ((frog frog)) 
  (not (plusp (pad-count frog))))

(defmethod splash ((frog frog)) nil)

(defmethod splash-maybe ((frog frog))
  (when (waterp frog)
    (splash frog)))

(defmethod max-speed ((frog frog))
  (if (home-to-mama-p frog)
      100
      (if (waterp frog)
	  *max-water-speed*
	  (if (carrying-fly-p frog) 
	      *max-carry-speed* 
	      *jump-speed*))))

(defun find-frogs ()
  (find-instances (pond) 'frog))

(defun find-mamas ()
  (find-instances (pond) 'mama))

(defun fly-carrier ()
  (find-if #'carrying-fly-p (find-frogs)))

(defmethod initialize-instance :after ((frog frog) &key)
  (resize frog *frog-size* *frog-size*))

(defparameter *frog-reload-frames* 30)

(defun frog-radius () (/ *frog-size* 1.8))

(defmethod collide :around ((this frog) (that frog))
  (multiple-value-bind (x y) (center-point this)
    (multiple-value-bind (x0 y0) (center-point that)
      (when (< (distance x0 y0 x y) (frog-radius))
	(call-next-method)))))

(defmethod repel :after ((this frog) (that frog) &optional speed)
  (play-sample (random-choose *bounce-sounds*)))

;;; Can't pass through walls

(defmethod collide ((wall wall) (frog frog))
  (impel frog :speed 10 :heading (heading-to-center frog)))

(defmethod collide ((brick brick) (frog frog))
  (impel frog :speed 10 :heading (heading-to-center frog)))

;;; Grab fly when you collide with it

(defmethod lose-fly ((frog frog))
  (with-slots (carrying) frog
    (setf carrying nil)))

(defun free-fly ()
  (dolist (frog (find-frogs))
    (lose-fly frog)))

(defmethod grab ((frog frog) &optional force)
  (when (or force (not (recently-kicked-p (fly))))
    (free-fly) 
    (play-sample (find-grab-sound frog))
    (with-slots (carrying) frog
      (setf carrying t))))

(defmethod collide ((frog frog) (fly fly))
  (unless (or (fly-carrier) (recently-kicked-p fly))
    (grab frog)))

;;; Glue the fly to the fly carrier, with some wobble

(defun wobble () (* 0.2 (sin (/ xelf:*updates* 10))))

(defun flashing-p ()
  (plusp (sin (/ *updates* 4))))

(defmethod carry-location ((frog frog))
  (with-slots (heading) frog
    (multiple-value-bind (cx cy) (center-point frog)
      (multiple-value-bind (tx ty) 
	  (step-coordinates cx cy heading (units 1))
	(multiple-value-bind (wx wy)
	    (step-coordinates tx ty (- heading (/ pi 2)) (* 30 (wobble)))
	  (values (- wx (* *fly-size* 0.12))
		  (- wy (* *fly-size* 0.12))))))))

(defmethod update :after ((fly fly))
  (let ((carrier (fly-carrier)))
    (if carrier
	(multiple-value-bind (x y) 
	    (carry-location carrier)
	  (center-on fly x y))
	(progn
	  (percent-of-time 1 (setf (slot-value fly 'heading) (random (* 2 pi))))
	  (forward fly 0.3)))))

;;; Default AI methods and tools

(defparameter *waypoint-margin* (units 4))

(defmethod find-waypoint ((frog frog) target)
  (center-point target))

(defmethod find-target ((frog frog)) 
  (cond ((carrying-fly-p frog)
	 (find-mama frog))
	((carrying-fly-p (teammate frog))
	 (nearest-opponent-to-teammate frog))
	((carrying-fly-p (opposing-striker frog))
	 (opposing-striker frog))
	((carrying-fly-p (opposing-guard frog))
	 (opposing-guard frog))
	((and (not (fly-carrier))
	      (> (distance-between frog (fly))
		 (distance-between (teammate frog) (fly))))
	 (nearest-opponent-to-teammate frog))
	(t (fly))))

(defmethod waypoint ((frog frog))
  (find-waypoint frog (find-target frog)))

(defmethod heading-to-opponent ((frog frog))
  (heading-between frog (opponent frog)))

(defmethod heading-to-fly ((frog frog))
  (if (fly) (heading-between frog (fly)) 0))

(defmethod distance-to-opponent ((frog frog))
  (distance-between frog (opponent frog)))

(defmethod distance-to-fly ((frog frog))
  (if (fly) (distance-between frog (fly)) 1000))

(defun jitter (heading)
  (+ heading (* (if (kids-mode-p) 0.2 0.15) (sin (/ *updates* 24)))))

(defmethod zonep ((thing thing)) nil)

(defun course-correction ()
  (if (not (kids-mode-p))
      (if (serve-period-p) 0.4 0.3)
      (if (serve-period-p) 0.35 0.25)))

(defmethod joystick-number ((frog frog)) nil)

(defmethod joystick-connected-p ((frog frog)) 
  (numberp (joystick-number frog)))

(defmethod stick-direction ((frog frog))
  (when (humanp frog)
    (let ((joystick (joystick-number frog)))
      (when joystick 
	(when (left-analog-stick-pressed-p joystick)
	  (find-heading-direction (left-analog-stick-heading joystick)))))))

(defmethod stick-heading ((frog frog))
  (let ((dir (stick-direction frog)))
    (when dir (direction-heading dir))))

(defmethod aim-heading ((frog frog))
  (when (and (game-on-p) (not *reset-clock*))
    (percent-of-time
	(if (serve-period-p)
	    (if (kids-mode-p) 80 90)
	    (if (and (zonep (fly))
		     (slow-p (fly)))
		;; anticipate eject but don't superspeed
		(if (kids-mode-p) 38 48)
		(if (and 
		     (not (carrying-fly-p frog))
		     (at-rest-p (fly))
		     (fly-within-range-p frog))
		    ;; slow down to catch fly
		    (if (slow-p frog) 60 55)
		    ;; default 
		    (if (not (kids-mode-p)) 68 78))))
      (when (and (not (colliding-with-p (fly) (mama-1))) 
		 (not (colliding-with-p (fly) (mama-2))))
	(multiple-value-bind (cx cy) (center-point frog)
	  (multiple-value-bind (wx wy) (waypoint frog)
	    (if (carrying-fly-p frog)
		(jitter (find-heading cx cy wx wy))
		(if (fast-p frog)
		    ;; correct path to not overshoot fly
		    (let ((delta (- (find-heading cx cy wx wy)
				    (trajectory-heading frog))))
		      (if (plusp delta)
			  (jitter (+ (find-heading cx cy wx wy) (course-correction)))
			  (jitter (- (find-heading cx cy wx wy) (course-correction)))))
		    (jitter (find-heading cx cy wx wy))))))))))

(defmethod trajectory-heading ((thing thing))
  (with-slots (x y last-x last-y) thing
    (find-heading last-x last-y x y)))

(defmethod fast-p ((thing thing))
  (with-slots (x y last-x last-y) thing
    (when (and last-x last-y)
      (> (distance last-x last-y x y)
	 3.4))))

(defmethod can-reach-fly ((self frog))
  (and (fly) (colliding-with-p self (fly))))

(defmethod fly-centered-p ((frog frog))
  (> 0.4 (abs (wobble))))

(defparameter *frog-shoot-distance* 320)

(defmethod jump-rate ((frog frog) x)
  (if (kids-mode-p) (/ x 2.6) (* 1.5 x)))

(defmethod jumping-p ((frog frog))
  (cond ((joystick-connected-p frog)
	  (holding-button-p (joystick-number frog)))
	((not (game-on-p)) nil)
	((carrying-fly-p frog)
	 (when (and (not (humanp frog))
		    (at-rest-p frog))
	   (percent-of-time (if (opposing-team-carrying-p frog) 
				(jump-rate frog 25) 
				(jump-rate frog 20)) t)))
	((not (carrying-fly-p frog))
	 (when (and (not (humanp frog))
		    (at-rest-p frog))
	   (percent-of-time (if (opposing-team-carrying-p frog) 
				(jump-rate frog 15)
				(jump-rate frog 10)) t)))))
	
(defmethod ready-to-jump-p ((frog frog)) 
  (zerop (slot-value frog 'jump-clock)))

(defparameter *jump-speed* 9)
(defparameter *long-jump-speed* 15)
(defparameter *steal-speed* 32)
(defparameter *carry-speed* 6)
(defparameter *kick-range* (units 2.8))

(defmethod fly-within-range-p ((frog frog))
  (< (distance-between frog (fly))
     *kick-range*))

(defparameter *repel-range* (units 4))

(defmethod jump ((frog frog) &optional long)
  (with-slots (image carrying jump-clock) frog
    (setf image *frog-jump-image*)
    (play-sample (random-choose *bounce-sounds*))
    (setf jump-clock *frog-reload-frames*)
    (let ((heading (or (aim-heading frog) (slot-value frog 'heading))))
      (impel frog :heading heading 
		  :speed (if (carrying-fly-p frog)
			     *carry-speed*
			     (if (not long) *jump-speed* *long-jump-speed*))))))
			 
(defmethod collide ((this frog) (that frog))
  (if (teammates-p this that)
      (progn nil)
	;; FIXME: pass
	;; (when (carrying-fly-p this) 
	;;   (grab that))
      (progn 
	(if (carrying-fly-p this)
	    (progn (free-fly)
		   (disable-kicking (fly))
		   (impel (fly) :speed 14 :heading (+ (random 2.0) (heading-between that this))))
	    (if (< (max-speed this) (max-speed that))
		(repel that this 7)
		(repel this that 7))))))

(defmethod update ((frog frog))
  (with-slots (image jump-clock) frog
    (when (slow-p frog)
      (setf image *frog-image*))
    (when (plusp jump-clock)
      (decf jump-clock))
    (when (and (ready-to-jump-p frog)
	       ;; whether to allow spamming the fly when you don't have it
	       (not (home-to-mama-p frog))
	       (jumping-p frog))
      (jump frog))))

;;; Mamas

(defclass mama (thing)
  ((color :initform "hot pink")
   (image :initform *frog-image*)))

(defmethod collide ((frog frog) (mama mama))
  (impel frog :speed 5 :heading (heading-to-center frog)))

(defmethod collide ((mama mama) (frog frog))
  (impel frog :speed 5 :heading (heading-to-center frog)))

(defmethod score-point ((mama mama)) nil)

(defmethod score-point :after ((mama mama))
  (play-sample "newball.wav")
  (play-sample "applause.wav"))

(defmethod collide ((mama mama) (frog frog))
  (impel frog :speed 10 :heading (heading-between mama frog)))

(defmethod collide ((mama mama) (fly fly))
  (when (game-on-p)
    (reset-physics fly)
    (when (not *reset-clock*)
      (score-point mama)
      (schedule-reset (current-buffer)))))

(defmethod find-children ((mama mama)) nil)

(defmethod collide :around ((mama mama) (fly fly))
  (let ((carrier (fly-carrier))
	(children (find-children mama)))
    (if (and carrier (or (object-eq carrier (first children))
			 (object-eq carrier (second children))))
	(call-next-method)
	(repel mama fly 15))))

(defparameter *mama-speed* 1)

(defmethod initialize-instance :after ((mama mama) &key)
  (resize mama (units 5) (units 3)))

(defmethod clear ((mama mama))
  (setf (slot-value mama 'timer) nil))

(defun clear-mamas ()
  (clear (mama-1))
  (clear (mama-2)))

(defparameter *mama-margin* (units 7))
(defparameter *mama-side-margin* 
  (let* ((margin *mama-margin*)
	 (usable-height (- *height* (* 2 margin)))
	 (usable-width usable-height))
    (* 0.5 (- *width* usable-width))))

(defparameter *mama-1-x* *mama-margin*)
(defparameter *mama-1-y* *mama-margin*)

(defparameter *mama-2-x* (- *width* *mama-margin*))
(defparameter *mama-2-y* (- *height* *mama-margin*))

(defclass mama-1 (mama) ((color :initform (mama-1-color))))
(defclass mama-2 (mama) ((color :initform (mama-2-color))))

(defmethod find-children ((mama mama-1)) (list (player-1) (player-2)))
(defmethod find-children ((mama mama-2)) (list (player-3) (player-4)))

(defmethod spawn-position ((mama mama-1)) (values *mama-1-x* *mama-1-y*))
(defmethod spawn-position ((mama mama-2)) (values *mama-2-x* *mama-2-y*))

(defmethod score-point ((mama mama-1)) (incf *score-1*))
(defmethod score-point ((mama mama-2)) (incf *score-2*))

(defmethod score-point :after ((mama mama-1))
  (setf *scoring-team* 1)
  (play-sample "fanfare-1.wav"))

(defmethod score-point :after ((mama mama-2))
  (setf *scoring-team* 2)
  (play-sample "fanfare-2.wav"))

;;; Frog subclasses 

(defparameter *goal-distance* 80)

(defmethod update :after ((frog frog))
  (when (and (carrying-fly-p frog)
	     (< (distance-between frog (find-mama frog))
		*goal-distance*))
    (collide (find-mama frog) (fly))))

(defmethod humanp ((frog frog)) 
  (or (input-p frog) 
      (keyboard-player-p (player-id frog))
      (joystick-connected-p frog)))

(defclass striker (frog) ())
(defmethod striker-p ((frog frog)) nil)
(defmethod striker-p ((striker striker)) t)

(defclass guard (frog) ())
(defmethod guard-p ((frog frog)) nil)
(defmethod guard-p ((guard guard)) t)

(defmethod jump-rate :around ((frog guard) x)
  (* (call-next-method)
     (if (opposing-team-carrying-p frog)
	 (if (kids-mode-p) 1.05 1.8)
	 1)))

(defmethod nearest-to ((frog frog) a b)
  (if (< (distance-between frog a)
	 (distance-between frog b))
      a b))

(defparameter *near-mama-distance* 250)

(defmethod near-mama-p ((frog frog))
  (< (distance-between frog (find-mama frog)) 
     *near-mama-distance*))

(defmethod near-opposing-mama-p ((frog frog))
  (< (distance-between frog (find-mama (opposing-striker frog)))
     *near-mama-distance*))

(defmethod x-disp ((frog frog)) 0)
(defmethod y-disp ((frog frog)) 0)

(defmethod guard-position ((frog frog))
  (multiple-value-bind (mx my) (center-point (find-mama frog))
    (multiple-value-bind (fx fy) (center-point (or (fly-carrier) (find-mama frog)))
      (values (/ (+ mx (x-disp frog) fx) 2)
	      (/ (+ my (y-disp frog) fy) 2)))))

(defmethod nearest-opponent-to-teammate ((frog frog))
  (destructuring-bind (a b) (find-opponents frog)
    (nearest-to (teammate frog) a b)))

(defmethod nearest-receiver ((frog frog))
  (nearest-to frog (teammate frog) (find-mama frog)))

(defmethod find-target :around ((frog guard))
  (or (cond ((carrying-fly-p frog)
	     (find-mama frog))
	    ((and (carrying-fly-p (teammate frog))
		  (not (near-mama-p frog)))
	     (nearest-opponent-to-teammate frog))
	    ((and (opposing-team-carrying-p frog)
		  (> (distance-between (fly-carrier) frog) 200)
	     (find-mama (opposing-striker frog)))))
      (call-next-method)))	

(defmethod waypoint :around ((frog guard))
  (if (and (opposing-team-carrying-p frog)
	   (not (near-mama-p frog))
	   (near-opposing-mama-p frog))
      (let ((danger (distance-between (fly-carrier) (find-mama (opposing-striker frog)))))
	(if 
	 (or 
	  (> (distance-between (fly-carrier) frog) 100)
	  (and
	   (< danger 300)
	   (< (distance-between frog (find-mama (opposing-striker frog)))
	      (+ 100 danger))))
	 (center-point (fly-carrier))
	 (guard-position (opposing-striker frog))))
      (call-next-method)))
	   
(defclass player-1 (striker) 
  ((color :initform *player-1-color*)
   (player-id :initform 1)))

(defclass player-2 (guard) 
  ((color :initform *player-2-color*)
   (player-id :initform 2)))

(defparameter *keyboard-player* nil)

(defun keyboard-player () *keyboard-player*)
(defun set-keyboard-player (player)
  (setf *keyboard-player* player))
(defsetf keyboard-player set-keyboard-player)

(defun keyboard-player-p (n) 
  (and (numberp (keyboard-player))
       (= (keyboard-player) n)))

;; (defmethod humanp ((frog player-1))
;;   (or (keyboard-player-p 1)
;;       (joystick-connected-p frog)))

(defclass player-3 (striker) 
  ((color :initform *player-3-color*)
   (player-id :initform 3)))

(defclass player-4 (guard) 
  ((color :initform *player-4-color*)
   (player-id :initform 4)))

(defmethod joystick-number ((frog player-1)) *player-1-joystick*)
(defmethod joystick-number ((frog player-2)) *player-2-joystick*)
(defmethod joystick-number ((frog player-3)) *player-3-joystick*)
(defmethod joystick-number ((frog player-4)) *player-4-joystick*)

(defmethod find-mama ((frog player-1)) (mama-1))
(defmethod find-mama ((frog player-2)) (mama-1))

(defmethod find-mama ((frog player-3)) (mama-2))
(defmethod find-mama ((frog player-4)) (mama-2))

(defmethod x-disp ((frog player-1)) (units 2))
(defmethod y-disp ((frog player-1)) (units 3))
(defmethod x-disp ((frog player-2)) (units 3))
(defmethod y-disp ((frog player-2)) (units 2))
(defmethod x-disp ((frog player-3)) (units -3))
(defmethod y-disp ((frog player-3)) (units -2))
(defmethod x-disp ((frog player-4)) (units -2))
(defmethod y-disp ((frog player-4)) (units -3))

(defmethod teammate ((frog player-1)) (player-2))
(defmethod teammate ((frog player-2)) (player-1))

(defmethod teammate ((frog player-3)) (player-4))
(defmethod teammate ((frog player-4)) (player-3))

(defmethod teammate-carrying-fly-p ((frog frog))
  (carrying-fly-p (teammate frog)))

(defmethod team-carrying-fly-p ((frog frog))
  (or (carrying-fly-p frog) 
      (teammate-carrying-fly-p frog)))

(defmethod teammates-p ((this frog) (that frog))
  (object-eq this (teammate that)))

(defmethod counterpart ((frog player-1)) (player-3))
(defmethod counterpart ((frog player-2)) (player-4))

(defmethod counterpart ((frog player-3)) (player-1))
(defmethod counterpart ((frog player-4)) (player-2))

(defmethod find-opposing-mama ((frog frog)) 
  (find-mama (counterpart frog)))

(defmethod opposing-guard ((frog player-1)) (player-4))
(defmethod opposing-guard ((frog player-2)) (player-4))

(defmethod opposing-guard ((frog player-3)) (player-2))
(defmethod opposing-guard ((frog player-4)) (player-2))

(defmethod opposing-striker ((frog player-1)) (player-3))
(defmethod opposing-striker ((frog player-2)) (player-3))

(defmethod opposing-striker ((frog player-3)) (player-1))
(defmethod opposing-striker ((frog player-4)) (player-1))

(defmethod find-opponents ((frog player-1)) (list (player-3) (player-4)))
(defmethod find-opponents ((frog player-2)) (list (player-3) (player-4)))
(defmethod find-opponents ((frog player-3)) (list (player-1) (player-2)))
(defmethod find-opponents ((frog player-4)) (list (player-1) (player-2)))

(defmethod find-grab-sound ((frog player-1)) "color1.wav")
(defmethod find-grab-sound ((frog player-2)) "color2.wav")
(defmethod find-grab-sound ((frog player-3)) "color3.wav")
(defmethod find-grab-sound ((frog player-4)) "color4.wav")

(defmethod opposing-team-carrying-p ((frog frog))
  (some #'carrying-fly-p (find-opponents frog)))

(defun frog-mama-distance ()
  (units 8))

(defmethod spawn-position ((frog player-1)) 
  (values (+ *mama-1-x* (frog-mama-distance)) *mama-1-y*))

(defmethod spawn-position ((frog player-2)) 
  (values *mama-1-x* (+ *mama-1-y* (frog-mama-distance))))

(defmethod spawn-position ((frog player-3)) 
  (values (- *mama-2-x* (frog-mama-distance)) *mama-2-y*))

(defmethod spawn-position ((frog player-4)) 
  (values *mama-2-x* (- *mama-2-y* (frog-mama-distance))))

;;; Lilypads

(defparameter *lilypad-width* (units 13))
(defparameter *lilypad-height* (units 6))

(defparameter *lilypad-small-width* (units 4))
(defparameter *lilypad-small-height* (units 2))

(defclass lilypad (thing) ((color :initform (random-choose *lilypad-colors*))))
(defclass lilypad-small (lilypad) ())

(defmethod auto-resize ((lilypad lilypad)) 
  (resize lilypad *lilypad-width* *lilypad-height*))

(defmethod auto-image ((lilypad lilypad))
  (setf (slot-value lilypad 'image) (random-choose *lilypad-images*)))

(defmethod auto-resize ((lilypad-small lilypad-small)) 
  (resize lilypad-small *lilypad-small-width* *lilypad-small-height*))

(defmethod auto-image ((lilypad-small lilypad-small))
  (setf (slot-value lilypad-small 'image) (random-choose *lilypad-small-images*)))

(defmethod initialize-instance :after ((lilypad lilypad) &key)
  (auto-image lilypad)
  (auto-resize lilypad))

(defmethod draw ((lilypad lilypad)) (draw-normally lilypad))

;;; Frogs should find lilypads on way home to mama

(defmethod next-lilypad ((frog frog) &optional goal)
  (let* ((lilypads (find-instances (pond) 'lilypad))
	 (mama (or goal (find-mama frog)))
	 ;; only examine pads in between here and mama
	 (path (remove-if #'(lambda (pad)
			      (< (distance-between pad mama)
				 (distance-between frog mama)))
			  lilypads)))
    (first (sort path #'< :key #'(lambda (pad)
				   (distance-between pad frog))))))

(defmethod lilypad-heading ((frog frog) &optional goal)
  (when (next-lilypad frog)
    (heading-between frog (next-lilypad frog goal))))

(defmethod lilypad-fly-heading ((frog frog))
  (lilypad-heading frog (fly)))

;;; Implementing water as background

(defmethod collide ((frog frog) (lilypad lilypad))
  (increase-pad-count frog))

;;; Islands with cat o'nine tails

(defclass island (thing)
  ((image :initform (random-choose *island-images*))))

(defmethod initialize-instance :after ((island island) &key)
  (resize island (units 3) (units 2)))

(defmethod draw ((island island)) (draw-normally island))

;;; Splash waves

(defclass wave (thing)
  ((image :initform *wave-image*)
   (clock :initform (random-choose '(100 120 140)))
   (color :initform "white")))

(defmethod draw ((wave wave))
  (with-slots (height width x y) wave
    (draw-textured-rectangle x y 0 width height 
			     (find-texture *wave-image*) 
			     :blend :alpha 
			     :vertex-color (random-choose '("cyan" "light sky blue")))))

(defmethod initialize-instance :after ((wave wave) &key)
  (resize wave 13 6))

(defmethod update ((wave wave)) 
  (with-slots (width height) wave
    (multiple-value-bind (x y) (center-point wave)
      (resize wave (+ width 0.8) (+ height 0.3))
      (when (find-hippo)
	(multiple-value-bind (wx wy) (step-coordinates x y 
						       (heading-between wave (find-hippo))
						       0.1)
	  (center-on wave wx wy)))))
  (with-slots (clock) wave
    (decf clock)
    (when (zerop clock)
      (destroy wave))))

(defmethod wave-location ((thing thing))
  (center-point thing))

(defmethod make-wave ((thing thing))
  (multiple-value-bind (x y) (wave-location thing)
    (let ((wave (make-instance 'wave)))
      (deploy wave)
      (center-on wave 
		 (* (+ x (random (units 1)) (random-choose '(-1 1))))
		 (* (+ y (random (units 0.5))) (random-choose '(-1 1)))))))

;;; Hippo

(defun random-hippo-times ()
  (list 
   (+ *updates* (seconds (random-choose '(9 14 19 28))))
   (+ *updates* 
      (seconds (random-choose '(45 55 70)))
      (random (seconds 30)))
   (+ *updates* 
      (minutes 2.5)
      (random (seconds 60)))))

(defvar *hippo-times* nil)

(defclass hippo (thing)
  ((image :initform *hippo-image*)
   (color :initform "white")
   (clock :initform (seconds 20))))

(defmethod wave-location ((hippo hippo))
  (multiple-value-bind (x y) (center-point hippo)
    (multiple-value-bind (wx wy) (step-coordinates x y 
						   (heading-between hippo (nearest-frog hippo))
						   (units (if (kids-mode-p) 0.3 0.6)))
      (values wx wy))))

(defmethod ensure-only-hippo ((hippo hippo))
  (let ((hippos (find-instances (pond) 'hippo)))
    (when (> 1 (length hippos))
      (dolist (h hippos)
	(when (not (object-eq h hippo))
	  (destroy h))))))

(defmethod initialize-instance :after ((hippo hippo) &key)
  (setf *hippo* hippo)
  (resize hippo (units 2.6) (units 1.4)))

(defmethod draw ((hippo hippo)) (draw-normally hippo))

(defmethod nearest-frog ((thing thing))
  (flet ((distance-from-here (x)
	   (distance-between x thing)))
    (first (sort (find-frogs) #'< :key #'distance-from-here))))

(defmethod update ((hippo hippo)) 
  (with-slots (clock) hippo
    (when (plusp clock)
      (decf clock))
    (if (zerop clock)
	(destroy hippo)
	(progn 
	  (move hippo 
		(heading-between hippo (nearest-frog hippo))
		(if (kids-mode-p) 0.3 0.7))
	  (percent-of-time 3.2 (make-wave hippo))))))

(defmethod destroy :after ((hippo hippo))
  (setf *hippo* nil))

(defmethod touching-lilypad-p ((thing thing))
  (block search
    (do-nodes (node (pond))
      (when (and (typep node (find-class 'lilypad))
		 (colliding-with-p node thing))
	(return-from search node)))))

(defmethod draw :around ((hippo hippo))
  (unless (touching-lilypad-p hippo)
    (call-next-method)))

(defmethod collide ((hippo hippo) (fly fly))
  (repel hippo fly))

(defmethod collide ((hippo hippo) (frog frog))
  (when (and (not (home-to-mama-p frog))
	     (not (touching-lilypad-p hippo)))
    (lose-fly frog)
    (bounce-home-to-mama frog)
    (with-slots (clock) hippo
      (setf clock (min clock 100)))))

;;; The frog pond

(defun make-wall (x y width height)
  (let ((wall (make-instance 'wall)))
    (xelf:resize wall width height)
    (xelf:move-to wall x y)
    wall))

(defun make-border (x y width height)
  (let ((left x)
	(top y)
	(right (+ x width))
	(bottom (+ y height)))
    ;; top wall
    (insert (make-wall left top (- right left) (units 1)))
    ;; bottom wall
    (insert (make-wall left bottom (- right left (units -1)) (units 1)))
    ;; left wall
    (insert (make-wall left top (units 1) (- bottom top)))
    ;; right wall
    (insert (make-wall right top (units 1) (- bottom top (units -1))))))

(defclass pond (buffer)
  ((resetting :initform nil)
   (ended :initform nil)
   (quadtree-depth :initform 7)))
   
(defvar *reset-clock* nil)

(defmethod quit-game ((pond pond))
  (quit))

(defmethod spacebar ((pond pond)) 
  (hide-terminal))

(defmethod initialize-instance :after ((pond pond) &key)
  (splash-screen-maybe)
  (setf *pond* pond)
  (resize pond *width* *height*)
  (bind-event pond '(:space) 'spacebar)
  (bind-event pond '(:return) 'spacebar)
  (bind-event pond '(:r :control) 'reset-game)
  (bind-event pond '(:k) 'kids-mode)
  (bind-event pond '(:k :control) 'kids-mode)
  ;; (bind-event pond '(:return) 'select-difficulty)
  (bind-event pond '(:escape) 'setup)
  (bind-event pond '(:q :control) 'quit-game))

(defmethod populate ((pond pond))
  (with-buffer pond
    (setf (slot-value (current-buffer) 'background-color) "slate blue")
    (make-border 0 0 (- *width* (units 1)) (- *height* (units 1)))
    (multiple-value-bind (x y) (center-point pond)
      (add-node (current-buffer) (make-instance 'fly) x y)
      (center-on (fly) x y))
    (set-player-1 (make-instance 'player-1))
    (set-player-2 (make-instance 'player-2))
    (set-player-3 (make-instance 'player-3))
    (set-player-4 (make-instance 'player-4))
    (set-mama-1 (make-instance 'mama-1))
    (set-mama-2 (make-instance 'mama-2))
    (mapc #'deploy (list (player-1) (player-2) (player-3) (player-4) (mama-1) (mama-2)))
    (face (mama-1) :right)
    (face (mama-2) :left)
    (let ((pad-1 (make-instance 'lilypad))
	  (island-1 (make-instance 'island))
	  (pad-2 (make-instance 'lilypad)))
      (mapc #'deploy (list pad-1 island-1 pad-2))
      ;;
      (multiple-value-bind (x y)
	  (spawn-position (mama-1))
	(center-on pad-1 x y))
      (multiple-value-bind (x y)
	  (center-point (current-buffer))
	(center-on island-1 x y))
      (multiple-value-bind (x y)
	  (spawn-position (mama-2))
	(center-on pad-2 x y))
      ;;
      (create-lilypads))
    ;; 
    (begin-serve-period)))

(defmethod kids-mode ((pond pond))
  (toggle-kids-mode))

(defmethod update ((pond pond))
  (let ((frogs (find-frogs)))
    (mapc #'reset-pad-count-maybe frogs)
    (call-next-method)
    (mapc #'splash-maybe frogs)))

(defmethod update :before ((pond pond))
  (clear-sounds)
  (when (and *hippo-times* 
	     (solop pond))
    (when (= *updates* (first *hippo-times*))
      (pop *hippo-times*)
      (percent-of-time 75      
	(let ((hippo (make-instance 'hippo)))
	  (deploy hippo)
	  (move-to hippo (/ *width* 2) (random-choose (list (- *height* 100) 30)))))))
  (update-game-clock)
  (update-serve-period-timer)
  (when (zerop *game-clock*)
    (when (null (slot-value pond 'ended))
      (setf (slot-value pond 'ended) t)
      (setf *reset-clock* nil)
      (play-sample "fanfare-3.wav"))))

(defmethod update :after ((pond pond))
  (update-terminal-timer)
  (when *reset-clock*
    (decf *reset-clock*)
    (when (zerop *reset-clock*)
      (reset-board pond))))

(defmethod schedule-reset ((pond pond))
  (setf *reset-clock* (seconds 3.5)))

(defun drop-fly ()
  (free-fly)
  (reset-physics (fly))
  (multiple-value-bind (x y) (center-point (current-buffer))
    (center-on (fly) x y)))

(defun add-lilypad (x y &optional (class 'lilypad))
  (let ((pad (make-instance class)))
    (deploy pad)
    (center-on pad x y)
    pad))

(defparameter *number-of-lilypads* 20)

(defun aspect-ratio ()
  (/ 1 (/ *width* *height*)))

(defparameter *lilypad-margin* (+ *mama-margin* (units 1)))

(defun create-lilypads ()
  (let ((x0 *lilypad-margin*)
	(y0 *lilypad-margin*)
	(x1 (- *width* *lilypad-margin*))
	(y1 (- *height* *lilypad-margin*)))
    (dotimes (n (truncate (/ *number-of-lilypads* 2)))
      (let* ((class (if (random-choose '(t t nil nil nil)) 'lilypad-small 'lilypad))
	     (pad-1 (add-lilypad 0 0 class))
	     (x (+ x0 (random (- x1 (slot-value pad-1 'width)))))
	     (y (+ y0 (random (- y1 (slot-value pad-1 'height))))))
	(center-on pad-1 x y)))))

(defun find-pond-class (&optional netplay)
  (case netplay
    (:client 'client-pond)
    (:server 'server-pond)
    (otherwise 'pond)))

(defun make-game (&optional netplay)
  (with-buffer (make-instance (find-pond-class netplay))
    (multiple-value-bind (x y) (center-point (current-buffer))
      (populate (current-buffer))
      (current-buffer))))

(defmethod reset-board ((pond pond))
  (setf *reset-clock* nil)
  (stop pond)
  (play-sample "go.wav")
  (switch-to-buffer (make-game *netplay*)))

(defmethod reset-game ((self pond))
  (mapc #'destroy (find-instances self 'hippo))
  (dotimes (n 100)
    (halt-sample n))
  (stop self)
  (reset-score)
  (reset-game-clock)
  (reset-board self)
  (at-next-update (destroy self)))

(defparameter *debug* nil)

(defmethod toggle-debug ((self pond))
  (setf *debug* (if *debug* nil t)))

(defmethod proceed ((pond pond))
  (drop-fly)
  (drop-player-1)
  (drop-player-2)
  (clear-mamas))

(defparameter *score-font* "sans-mono-bold-32")
(defparameter *status-font* "sans-mono-bold-12")

(defparameter *big-font* "sans-mono-bold-16")

(defmethod draw :before ((pond pond))
  (draw-box 0 0 *width* *height* :color "slate blue"))

(defmethod draw :after ((pond pond))
  ;; (mapc #'draw (get-nodes pond))
  (mapc #'draw (find-instances pond 'hippo))
  (mapc #'draw (find-instances pond 'lilypad))
  (mapc #'draw (find-mamas))
  (mapc #'draw (find-instances pond 'island))
  (mapc #'draw (find-frogs))
  (when (fly) (draw (fly)))
  (draw-string (format nil "~S" *score-1*)
	       (units 2) 3
	       :color (if (and *reset-clock* (= 1 *scoring-team*)) (random-choose '("yellow" "light cyan" "orchid" "magenta")) *player-1-color*)
	       :font *score-font*)
  (draw-string (format nil "~S" *score-2*)
	       (- *width* (units 5)) 3
	       :color (if (and *reset-clock* (= 2 *scoring-team*)) (random-choose '("yellow" "light cyan" "orchid" "magenta")) *player-3-color*)
	       :font *score-font*)
  (draw-string (game-clock-string) 
	       (units 31.6) 3
	       :color "white"
	       :font *status-font*)
  (draw-string (format nil "[Escape] Set up players/gamepads/network    [Control-R] reset game     [Control-Q] quit     [Control-K] Toggle kids easy mode: ____    ~A" (or *netplay* ""))
	       (units 5.6) (- *height* 17)
	       :color "white"
	       :font *status-font*)
  (unless (game-on-p)
    (draw-string "END OF REGULATION"
		 (units 36) (units 2)
		 :color "white"
		 :font *big-font*))
  (when (kids-mode-p)
    (draw-textured-rectangle (units 50.5) (- *height* (units 1.2)) 0 
			     (units 1) (units 1) (find-texture *teddy-image*)
			     :blend :alpha :vertex-color *teddy-color*))
  (draw-terminal-maybe 10 t))

;;; Preventing mousey movements

(defmethod handle-point-motion ((self pond) x y))
(defmethod press ((self pond) x y &optional button))
(defmethod release ((self pond) x y &optional button))
(defmethod tap ((self pond) x y))
(defmethod alternate-tap ((self pond) x y))

;;; Netplay integration

(defmethod quadrille:find-identifier ((thing thing))
  (xelf:make-keyword (xelf:uuid thing)))

(defun find-thing-from-id (id)
  (xelf:find-object (symbol-name id) t))

(setf quadrille:*identifier-search-function* 
      #'find-thing-from-id)

(defmethod make-census ((pond pond))
  (let ((uuids (make-hash-table :test 'equal :size 64)))
    (do-nodes (node pond)
      (setf (gethash (slot-value node 'xelf::uuid) uuids)
	    (slot-value node 'xelf::uuid)))
    (setf *census* uuids)
    ;;(verbosely "Created census with ~S uuids." (hash-table-count uuids))
    uuids))

(setf xelf:*game-variables*
      '(xelf:*updates* *kids-mode*
	*keyboard-player* *game-clock* *score-1* *score-2* *scoring-team*
	*serve-period-timer* *hippo-times* *reset-clock* *paused*
	*degrade-stream-p* xelf::*server-sent-messages-count*))

(setf xelf:*object-variables* 
      '(*player-1* *player-2* *hippo*
		  *player-3* *player-4* *mama-1* *mama-2* *fly*))

(setf xelf:*safe-variables* 
      (append *game-variables* 
	      *object-variables* 
	      xelf:*other-variables*))

(setf xelf:*terminal-bottom* (- *height* (units 1.5)))

(setf xelf:*prompt-font* xelf:*terminal-font*)

(setf xelf:*terminal-left* (units 1))

(defmethod find-player ((pond pond) n)
  (ecase n
    (1 (player-1))
    (2 (player-2))
    (3 (player-3))
    (4 (player-4))))

(defclass client-pond (client-buffer pond) ())

(defmethod find-input ((frog frog))
  (list 
   :time (current-time)
   :player-id (slot-value frog 'player-id)
   :stick-direction (stick-direction frog)
   :jumping-p (jumping-p frog)))
	
(defmethod find-local-inputs ((pond client-pond))
  (mapcar #'find-input (remove-if-not #'humanp (find-frogs))))

(defmethod update-input-state :after ((frog frog) plist time)
  (destructuring-bind (&key stick-direction jumping-p player-id time) plist
    (setf (input-direction frog) stick-direction)
    (setf (input-jumping-p frog) jumping-p)))

(defmethod jumping-p :around ((frog frog))
    (cond ((and (serverp (pond))
		(input-p frog) 
		(input-update-p frog))
	   (input-jumping-p frog))
	  ((keyboard-player-p (player-id frog))
	   (or (holding-shift-p) 
	       (call-next-method)))
	  (t (call-next-method))))

(defmethod stick-direction :around ((frog frog))
  (if (and 
       (not (clientp (current-buffer)))
       (input-p frog)
       (input-update-p frog))
      (input-direction frog)
      (if (keyboard-player-p (player-id frog))
	  (or (arrow-keys-direction) 
	      (call-next-method))
	  (call-next-method))))

;;; Server pond class

(defclass server-pond (server-buffer pond) ())

(defmethod background-stream ((pond server-pond))
  (mapc #'(lambda (x) (slot-value x 'xelf::uuid))
	(find-instances pond 'lilypad)))

(defmethod ambient-stream ((pond server-pond))
  (let ((stream (copy-tree (make-node-stream))))
    (dolist (var *object-variables*)
      (let ((thing (symbol-value var)))
	(when (or (find thing stream :test #'object-eq)
		  (typep thing (find-class 'frog))
		  (typep thing (find-class 'wall))
		  (typep thing (find-class 'fly))
		  (typep thing (find-class 'hippo))
		  (typep thing (find-class 'mama)))
	  (setf stream (delete (slot-value thing 'xelf::uuid)
			       stream))
	  (verbosely "Removed ~S from ambient stream." thing))))
    stream))

;;; 2fr0g.lisp ends here
