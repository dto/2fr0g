(in-package :2fr0g)

;;; Controller screen

(defparameter *button-time* 30)
(defparameter *ready-time* 120)

(defclass setup (buffer)
  ((timer :initform 0)
   (player :initform 1)
   (quadtree-depth :initform 4)
   (background-color :initform "CornflowerBlue")))

(defmethod toggle-upnp ((setup setup))
  (setf *use-upnp* (if *use-upnp* nil t)))

(defmethod start-server ((setup setup))
  (play-2fr0g :netplay :server 
	      :use-upnp *use-upnp*))

(defmethod start-client-prompt ((setup setup))
  (show-prompt))

(defmethod start-client ((setup setup))
  (play-2fr0g :netplay :client 
	      :use-upnp *use-upnp* 
	      :server-host *server-host*))

(defconstant +first-frog+ 1)
(defvar *frog-number* nil)

(defun next-frog () 
  (when (numberp *frog-number*) (setf *frog-number* (1+ (mod *frog-number* 4)))))

(defmethod initialize-instance :after ((setup setup) &key)
  (bind-event setup '(:s :control) 'start-server)
  (bind-event setup '(:c :control) 'start-client-prompt)
  (bind-event setup '(:u :control) 'toggle-upnp)
  (when (null (player-1))
    (set-player-1 (make-instance 'player-1))
    (set-player-2 (make-instance 'player-2))
    (set-player-3 (make-instance 'player-3))
    (set-player-4 (make-instance 'player-4)))
  (setf (keyboard-player) nil)
  (setf *player-1-joystick* nil)
  (setf *player-2-joystick* nil)
  (setf *player-3-joystick* nil)
  (setf *player-4-joystick* nil)
  (setf *inhibit-splash-screen* t)
  (setf *frog-number* +first-frog+)
  (hide-terminal)
  (resize setup *width* *height*))

(defmethod update :after ((setup setup))
  (with-slots (timer player) setup
    (when (plusp timer) 
      (decf timer))
    (when (and (zerop timer)
	       (null player))
      (at-next-update 
	(switch-to-buffer (make-game *netplay*))))))

(defun ready-prompt () "Get Ready!")

(defun ai-or-net ()
  (if *netplay* "AI or Network" "AI"))

(defun label-frog (n &optional flash)
  (multiple-value-bind (x y) (center-point (find-player (pond) n))
    (draw-string (format nil "~S  ~A" n
			 (if (humanp (find-player (pond) n))
			     (or (cond ((joystick-number (find-player (pond) n))
					"Human (gamepad)")
				       ((and (keyboard-player) (= n (keyboard-player)))
					"Human (Keyboard)"))
				 (ai-or-net))
			     (ai-or-net)))
		 (- x (units 3)) (+ y (units 1.2))
		 :color (if flash (random-choose '("white" "cyan" "yellow")) "white")
		 :font "sans-mono-bold-16")))

(defun flicker () (random-choose '("white" "cyan")))

(defparameter *info* " [N] Choose next frog   [Gamepad button] Assign gamepad to this frog   [K] Assign PC Keyboard to this frog   [Enter] Apply changes and Play    [W] War without humans! ")

(defparameter *info-2* "(Keyboard player can also use Arrows/Numpad and Shift keys. Gamepads must be connected before starting the application.)")

(defun netplay-setup-string ()
  (if *netplay* 
      (format nil "Netplay: ~S" *netplay*) 
      "Network play not configured. See http://xelf.me/netplay.html for command line configuration instructions."))

(defparameter *net* "Online Play: Press Control-S to start server,  Control-C for client. Press Control-U to toggle UPnP before starting.")

(defmethod draw :after ((setup setup))
  (when (null *prompt*)
    (with-slots (timer player button) setup
      (draw (find-player (pond) 1))
      (draw (find-player (pond) 2))
      (draw (find-player (pond) 3))
      (draw (find-player (pond) 4))
      (spawn (find-player (pond) 1))
      (spawn (find-player (pond) 2))
      (spawn (find-player (pond) 3))
      (spawn (find-player (pond) 4))
      (label-frog 1)
      (label-frog 2)
      (label-frog 3)
      (label-frog 4)
      (label-frog *frog-number* :flash)
      (draw-string "Choose players/controllers" (units 1) (units 1) :color "white" :font "sans-mono-bold-22")
      (draw-string *info* (units 1) (units 3) :color "white" :font "sans-mono-bold-12")
      (draw-string *info-2* (units 1) (units 4) :color "white" :font "sans-mono-bold-12")
      (draw-string (if *use-upnp* "UPnP Enabled" "UPnP Disabled") (units 52) (units 6) :color "white" :font "sans-mono-bold-12")
      (draw-string 
       (if *netplay* 
	   (format nil "Online play enabled in ~S mode." *netplay*)
	   *net*)
       (units 10) (units 5) :color "white" :font "sans-mono-bold-12")
      (when (null player)
	(draw-string (ready-prompt) (units 12) (units 18) :color "white" :font *big-font*))))
  (when *prompt*
    (draw *prompt*)))

(defmethod handle-event :after ((setup setup) event)
  (with-slots (timer player) setup
    (when (and (zerop timer)
	       (listp (first event)))
      ;; not a joystick event
      (destructuring-bind (head &rest modifiers) event
	(when (consp head)
	  (cond ((or (eq :space (first head))
		     (eq :return (first head)))
		 (reset-game-clock)
		 (setf *reset-clock* nil)
		 (setf player nil))
		((eq :k (first head))
		 (setf (keyboard-player) *frog-number*)
		 (next-frog))
		((eq :w (first head))
		 (setf (keyboard-player) nil)
		 (setf *player-1-joystick* nil) 
		 (setf *player-2-joystick* nil)
		 (setf *player-3-joystick* nil) 
		 (setf *player-4-joystick* nil)
		 (reset-game-clock)
		 (setf *reset-clock* nil)
		 (setf player nil))
		((eq :n (first head))
		 (next-frog))))))
    (when (and (eq :joystick (first event))
	       (not (plusp timer)))
      (setf timer *button-time*)
      (destructuring-bind (which new-button dir) (rest event)
	(case *frog-number*
	  (1 (setf *player-1-joystick* which))
	  (2 (setf *player-2-joystick* which))
	  (3 (setf *player-3-joystick* which))
	  (4 (setf *player-4-joystick* which)))
	(next-frog)))))

(defmethod handle-event :around ((setup setup) event)
  (if *prompt*
      (prog1 t (handle-event *prompt* event))
      (call-next-method)))

(defmethod setup ((pond pond))
  (stop pond)
  (hide-terminal)
  (when *netplay* 
    (close-netplay))
  (at-next-update 
    (switch-to-buffer (make-instance 'setup))))

;;; Main program

(defparameter *title-string* "2fr0g (beta) 0.99")

(defun play-2fr0g (&key (use-upnp nil) (netplay *netplay*) (server-host *server-host*) (client-host *client-host*) (base-port *base-port*) verbose-logging)
  (setf *ip-address* nil)
  (reset-score)
  (when netplay 
    (close-netplay))
  (setf xelf::*use-upnp* use-upnp)
  (setf *degrade-stream-p* nil)
  (setf *server* nil)
  (setf *client* nil)
  (setf *verbose-p* verbose-logging)
  (setf *server-port* (or base-port *base-port*))
  (setf *netplay* netplay)
  (setf *server-host* server-host)
  (setf *upnp-client-host* client-host)
  (setf *client-host* client-host)
  (setf *sent-messages-count* 0)
  (setf *received-messages-count* 0)
  (setf *remote-host* nil)
  (setf *remote-port* nil)
  (setf *last-message-timestamp* 0)
  (setf *flag-received-p* nil)
  (setf *flag-sent-p* nil)
  (reset-game-clock)
  (setf *reset-clock* nil)
  (setf *kids-mode* nil)
  (install-frog-colors *pastel-frog-colors*)
  (setf *echo-timer* 0)
  (disable-key-repeat) 
    (dolist (line (split-string-on-lines *2fr0g-copyright-notice*))
      (logging line))
    (show-terminal)
    (initialize-sounds)
  (unless *inhibit-splash-screen*
    (logging "PRESS [ESCAPE] KEY TO CONFIGURE PLAYERS, CONTROLLERS, AND NETWORKING."))
  (at-next-update
    (play-sample "go.wav")
    (switch-to-buffer (make-game netplay))))

(defun 2fr0g (&rest args)
  (setf *paused* nil)
  (setf *fullscreen* nil)
  (setf *font-texture-scale* 2)
  (setf *font-texture-filter* :linear)
  (setf *window-title* *title-string*)
  (setf *screen-width* *width*)
  (setf *screen-height* *height*)
  (setf *nominal-screen-width* *width*)
  (setf *nominal-screen-height* *height*)
  (setf *scale-output-to-window* t) 
  (setf *default-texture-filter* :nearest)
  (setf *use-antialiased-text* nil)
  (setf *frame-rate* +60fps+)
  (setf *inhibit-splash-screen* nil)
  (setf (keyboard-player) nil)
  (setf *player-1-joystick* nil)
  (setf *player-2-joystick* nil)
  (setf *player-3-joystick* nil)
  (setf *player-4-joystick* nil)
  (setf *netplay* nil)
  (setf *terminal-lines* nil)
  (with-session 
    (open-project "2fr0g")
    (index-all-images)
    (index-all-samples)
    (index-pending-resources)
    (preload-resources)
    (setf *hippo-times* (random-hippo-times))
    (apply #'play-2fr0g args)))
